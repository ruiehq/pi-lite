const express = require('express');
const router = express.Router();

require('../relays');

router.post('/on', function(req, res){
   relayOne.writeSync(0, function() {
      console.log('button is on')
   })
	res.render('index', {status: "Water Pump is On", data: datas});
});

router.post('/off', function(req, res){
   relayOne.writeSync(1, function() {
      console.log('button is off')
   })
	res.render('index', {status: "Water Pump is Off", data: datas});
});

module.exports = router;
