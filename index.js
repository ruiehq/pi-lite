const express = require('express');
const path = require('path');
const app = express();
const firebase = require('firebase');

const config = { databaseURL: "https://aeropi-6df86.firebaseio.com" };
firebase.initializeApp(config);


var onoff = require('onoff');
const io = onoff.Gpio;
let relayOne = new io(22, 'out');
let relayTwo = new io(23, 'out');
let relayThree = new io(24, 'out');

app.set('view engine', 'ejs')
app.set('views', path.join(__dirname, 'views'));

app.use(express.static(path.join(__dirname, 'public')));

const serialport = require("serialport");
const Readline = serialport.parsers.Readline
const port = new serialport('/dev/ttyUSB0');

const parser = new Readline();
port.pipe(parser);

let sensorH = firebase.database().ref('sensors/hourly');
var datas;
let relays;
let sensorHourly = {}; 

port.on('open', () => {
  console.log('Serial is Open');
});

parser.on('data', function (data) {
   datas = JSON.parse(data);
   console.log('Sensors:', datas)
});

sensorH.orderByChild('timestamp').limitToLast(5).once('value', data => {
	sensorHourly = data.val();
});

loopDB();
loop();
checkTemp(datas);

relayOne.writeSync(1, function() {
	console.log('Relay 1 is on')
})

relayTwo.writeSync(1, function() {
	console.log('Relay 2 is on')
})

relayThree.writeSync(1, function() {
	console.log('Relay 3 is on')
})



app.get('/', (req, res) => {
	res.render('index',{status:"Press Button To change Status of Water Pump !!", data: datas});
});

app.get('/hourly', (req, res) => {
	let objArr = [];
	let arrHumid = [];
	let arrTemp = [];
	let arrPh = [];
	for(data in sensorHourly){
		objArr.push(sensorHourly[data]);
	}

	objArr.forEach(data => {
		if(data['humidity']){
			arrHumid.push(data['humidity']);
		}
		if(data['temperature']){
			arrTemp.push(data['temperature']);
		}
		if(data['phlvl']){
			arrPh.push(data['phlvl']);
		}
	});
	
   res.json({ data: { humidity: arrHumid, temperature: arrTemp, phlvl: arrPh }});
})

app.post('/relayOne/on', function(req, res){
   relayOne.writeSync(0, function() {
      console.log('button is on')
   })
   res.json({ message: 'success' })
   //res.render('index', {status: "Water Pump is On", data: datas});
});

app.post('/relayOne/off', function(req, res){
   relayOne.writeSync(1, function() {
      console.log('button is off')
   })
   res.json({ message: 'success' })
   //res.render('index', {status: "Water Pump is Off", data: datas});
});

app.post('/relayTwo/on', function(req, res){
   relayTwo.writeSync(0, function() {
      console.log('button is on')
   })
	res.render('index', {status: "Water Pump is On", data: datas});
});

app.post('/relayTwo/off', function(req, res){
   relayTwo.writeSync(1, function() {
      console.log('button is off')
   })
	res.render('index', {status: "Water Pump is Off", data: datas});
});

app.post('/relayThree/on', function(req, res){
   relayThree.writeSync(0, function() {
      console.log('button is on')
   })
	res.render('index', {status: "Water Pump is On", data: datas});
});

app.post('/relayThree/off', function(req, res){
   relayThree.writeSync(1, function() {
      console.log('button is off')
   })
	res.render('index', {status: "Water Pump is Off", data: datas});
});

app.get('/api', (req, res) => {
   res.send(datas);
})

app.listen(3000, () => {
   console.log(`System is running`);
})

function sendSensorHourly(value) {
   let sensorRef = firebase.database().ref('sensors/hourly');
   let { humidity, temperature, waterlvl, phlvl } = value
      sensorRef.push({temperature: {
         x: firebase.database.ServerValue.TIMESTAMP,
         y: temperature
      },
      humidity: {
         x: firebase.database.ServerValue.TIMESTAMP,
         y: humidity
      },
      waterlvl: {
         x: firebase.database.ServerValue.TIMESTAMP,
         y: waterlvl
      },
      phlvl:{
         x: firebase.database.ServerValue.TIMESTAMP,
         y: phlvl
      }})
      .then(() => console.log('Hourly Data: OK'))
      .catch(() => console.log('Hourly Data: Error'))
}

function checkWater(data) {
   let { humidity, temperature, waterlvl, phlvl } = data;
   if(waterlvl === "low") {
      // Run Nutrients Pump
      relayTwo.writeSync(0, function() {
         console.log('Nutrients On')
      })
   }
   
   if(waterlvl === "normal") {
      relayTwo.writeSync(1, function() {
         console.log('Nutrients Off')
      })
   }
	 
   if(waterlvl === "error") {
      relayTwo.writeSync(1, function() {
         console.log('Nutrients Off')
      })
   }
}

function checkTemp() {
   let timing = 5000;

	if(value) {
	   let { temperature } = datas;
console.log('value test',value);
	   switch(temperature) {
	      case (temperature >= 26 && temperature <= 28):
		 timing = 20000; 
		 break;
	      case (temperature >= 28 && temperature <= 31):
		 timing = 15000;
		 break;
	      case (temperature >= 31 && temperature <= 34):
		 timing = 10000;
		 break;
	      case (temperature >= 34 && temperature <= 36):
		 timing = 5000;
		 break;
	      default:
		 timing = 3000;
	   }
	}
	console.log('timing', timing);

	setTimeout(checkTemp, timing);
}


function checkPh(data) {
   let { humidity, temperature, waterlvl, phlvl } = data;
   if(phlvl < 6) {
      relayThree.writeSync(1)
   }
   
   if(phlvl > 5) {
      relayThree.writeSync(0)
   }
}


function updateDB() {
   sendSensorHourly(datas);
   sensorH.orderByChild('timestamp').limitToLast(5).once('value', data => {
      sensorHourly = data.val();
   });
   console.log('Hourly DB Sent')
}

function loopDB() {
	if(datas) {
		updateDB();	
	}
	setTimeout(loopDB, 15 * 1000);
}

function loop() {
	if(datas) {
	      checkWater(datas);
	      checkPh(datas);
         relayOne.writeSync(0)
	}
	setTimeout(loop, 1 * 1000);
}

process.on('SIGINT', function () {
  relayOne.writeSync(1);
  relayOne.unexport();
  console.log('Bye, bye!');
  process.exit();
});
